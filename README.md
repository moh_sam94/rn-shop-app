## Overview

This project was implemented from [Udemy React Native Course](https://www.udemy.com/share/101Wau3@iGs0P5XftvoMyDs9WsDnPdUdoniJwG2RXyTEwjxws-XNbSotSQE5EsNSv5UZaXqv/).
It is an app where the use is able to add/edit and delete a product, add it to the cart and create an order.

## Dependencies

- node

## firebase Database:

- A google account is required for this operation.
- create a realtime firebase databases.
- Create a web app in firebase console and connect it to the database.
- Enable sign up with email/password in firebase authentications section.
- fill in the constats/firebase.js file with your credentials

* FIREBASE_API_KEY=
* FIREBASE_DATABASE_URL=

## Install

`npm install`

### Start

`npm run start`

### Android

`npm run android`

### ios

`npm run ios`
